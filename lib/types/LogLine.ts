export type LogLine = string | Error | unknown | (() => string | Error | unknown);
