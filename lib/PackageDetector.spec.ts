import '@ayana/test';

import { PackageDetector } from './PackageDetector';

describe('PackageDetector', function() {
	describe('#getCallerDirectory', function() {
		it('should return the callers directory', function() {
			const tested = new PackageDetector();

			const expectedPath = '/home/julian/Workspaces/ayana/logger/lib/PackageDetector.spec.ts';

			// We need to wrap the call once again so we simulate getting our caller
			const dir = (function() {
				return tested.getCallerFile();
			})();

			expect(dir, 'to be', expectedPath);
		});
	});

	describe('#getInfo', function() {
		const getClean = () => {
			const tested = new PackageDetector();

			tested.readFileSync = sinon.fake();

			return tested;
		};

		it('should load the package.json file if it is not in the cache, add it to the cache and return it', function() {
			const tested = getClean();

			const pkg = { some: 'json' };

			tested.readFileSync = sinon.fake.returns(JSON.stringify(pkg));

			const result = tested.getInfo('/');

			expect(result, 'to have properties', pkg);
			expect(tested.packageCache.get('/package.json'), 'to have properties', pkg);

			sinon.assert.calledOnce(tested.readFileSync as any);
		});

		it('should not load the package.json file if it is in the cache and return it', function() {
			const tested = getClean();

			const pkg = {};
			tested.packageCache.set('/package.json', pkg);

			const result = tested.getInfo('/');

			expect(result, 'to be', pkg);

			sinon.assert.notCalled(tested.readFileSync as any);
		});
	});

	describe('#getRootOf', function() {
		const getClean = () => {
			const tested = new PackageDetector();

			tested.readdirSync = sinon.fake();

			return tested;
		};

		it('should find the next folder containing a package.json file', function() {
			const tested = getClean();

			const expectedPath = '/home/ayana/Workspaces/ayana/logger';

			tested.readdirSync = function(path) {
				if (path === expectedPath) {
					return ['package.json'];
				}

				return [];
			};

			const root = tested.getRootOf('/home/ayana/Workspaces/ayana/logger/some/very/fancy/dir');

			expect(root, 'to be', expectedPath);
		});

		it('should abort after checking the root directory for a package.json', function() {
			const tested = getClean();

			tested.readdirSync = sinon.fake.returns([]);

			expect(
				() => tested.getRootOf('/'),
				'to throw',
				'No package.json could be found in the directory tree'
			);
		});
	});
});
